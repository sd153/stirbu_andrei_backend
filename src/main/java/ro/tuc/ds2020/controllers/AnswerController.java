package ro.tuc.ds2020.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dto.AnswerDTO;
import ro.tuc.ds2020.entities.Answer;
import ro.tuc.ds2020.entities.Question;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.services.AnswerService;
import ro.tuc.ds2020.services.QuestionService;
import ro.tuc.ds2020.services.UserService;


import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/answer")
@RequiredArgsConstructor
@CrossOrigin
public class AnswerController {

    private final AnswerService answerService;
    private final QuestionService questionService;
    private final UserService userService;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    @ResponseBody
    public List<AnswerDTO> getAnswer() {
        List<Answer> answers = answerService.getAllAnswers();
        List<AnswerDTO> answerDTOS = new ArrayList<>();

        for(Answer answer : answers) {
            AnswerDTO answerDTO = AnswerDTO.builder()
                    .id(answer.getId())
                    .userID(answer.getUserAnswer().getId())
                    .questionID(answer.getQuestion().getId())
                    .answerVotes(answer.getAnswerVotes())
                    .answerText(answer.getAnswerText())
                    .answerDate(answer.getAnswerDate())
                    .build();

            answerDTOS.add(answerDTO);
        }

        return answerDTOS;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAll/{questionId}")
    @ResponseBody
    public List<AnswerDTO> getAnswersByQuestionId(@PathVariable(name = "questionId") long questionId) {
        List<Answer> answers = answerService.getAllAnswersByQuestionId(questionId);
        List<AnswerDTO> answerDTOS = new ArrayList<>();

        for(Answer answer : answers) {
            AnswerDTO answerDTO = AnswerDTO.builder()
                    .id(answer.getId())
                    .userID(answer.getUserAnswer().getId())
                    .questionID(answer.getQuestion().getId())
                    .answerVotes(answer.getAnswerVotes())
                    .answerText(answer.getAnswerText())
                    .answerDate(answer.getAnswerDate())
                    .build();

            answerDTOS.add(answerDTO);
        }

        return answerDTOS;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAnswer/{id}")
    @ResponseBody
    public AnswerDTO getAnswer(@PathVariable(name = "id") long id) {
        Answer answer = answerService.getAnswer(id);
        AnswerDTO answerDTO = AnswerDTO.builder()
                .id(answer.getId())
                .userID(answer.getUserAnswer().getId())
                .questionID(answer.getQuestion().getId())
                .answerVotes(answer.getAnswerVotes())
                .answerText(answer.getAnswerText())
                .answerDate(answer.getAnswerDate())
                .build();

        return  answerDTO;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/deleteAnswer/{id}")
    @ResponseBody
    public String deleteAnswer(@PathVariable(name = "id") long id) {
        return answerService.deleteAnswer(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/saveAnswer")
    @ResponseBody
    public AnswerDTO saveAnswer(@RequestBody AnswerDTO answerDTO) {
        Question question = questionService.getQuestion(answerDTO.getQuestionID());
        User user = userService.getUser(answerDTO.getUserID());

        Answer answer = Answer.builder()
                .userAnswer(user)
                .question(question)
                .answerVotes(answerDTO.getAnswerVotes())
                .answerText(answerDTO.getAnswerText())
                .answerDate(answerDTO.getAnswerDate())
                .build();
        Answer savedAnswer = answerService.saveAnswer(answer);

        AnswerDTO savedAnswerDTO = AnswerDTO.builder()
                .id(savedAnswer.getId())
                .userID(savedAnswer.getUserAnswer().getId())
                .questionID(savedAnswer.getQuestion().getId())
                .answerVotes(savedAnswer.getAnswerVotes())
                .answerText(savedAnswer.getAnswerText())
                .answerDate(savedAnswer.getAnswerDate())
                .build();

        return savedAnswerDTO;
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/updateAnswer")
    @ResponseBody
    public AnswerDTO updateAnswer(@RequestBody AnswerDTO answerDTO) {
        Question question = questionService.getQuestion(answerDTO.getQuestionID());
        User user = userService.getUser(answerDTO.getUserID());
        Answer answer = Answer.builder()
                .id(answerDTO.getId())
                .userAnswer(user)
                .question(question)
                .answerVotes(answerDTO.getAnswerVotes())
                .answerText(answerDTO.getAnswerText())
                .answerDate(answerDTO.getAnswerDate())
                .build();
        Answer updatedAnswer = answerService.updateAnswer(answer.getId(),answer);

        AnswerDTO updatedAnswerDTO = AnswerDTO.builder()
                .id(updatedAnswer.getId())
                .userID(updatedAnswer.getUserAnswer().getId())
                .questionID(updatedAnswer.getQuestion().getId())
                .answerVotes(updatedAnswer.getAnswerVotes())
                .answerText(updatedAnswer.getAnswerText())
                .answerDate(updatedAnswer.getAnswerDate())
                .build();

        return updatedAnswerDTO;
    }

    @GetMapping("/count/{questionId}")
    public ResponseEntity<Long> countAnswersByQuestionId(@PathVariable(name = "questionId") long questionId) {
        return ResponseEntity.ok(answerService.countAnswersByQuestionId(questionId));
    }
}
