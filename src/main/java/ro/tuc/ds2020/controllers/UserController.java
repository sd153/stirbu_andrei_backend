package ro.tuc.ds2020.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dto.LoginDTO;
import ro.tuc.ds2020.dto.UserDTO;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.services.UserService;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/users")
@CrossOrigin
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    @ResponseBody
    public List<UserDTO> getUser() {
        List<User> users = userService.getAllUsers();
        List<UserDTO> userDtos = new ArrayList<>();

        for(User user : users) {
            UserDTO userDTO = UserDTO.builder()
                    .id(user.getId())
                    .username(user.getUsername())
                    .password(user.getPassword())
                    .name(user.getName())
                    .isAdmin(user.getIsAdmin())
                    .isBanned(user.getIsBanned())
                    .score(user.getScore())
                    .build();

            userDtos.add(userDTO);
        }

        return userDtos;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getUser/{id}")
    @ResponseBody
    public UserDTO getUser(@PathVariable(name = "id") long id) {
        User user = userService.getUser(id);
        UserDTO userDTO = UserDTO.builder()
                .id(user.getId())
                .username(user.getUsername())
                .password(user.getPassword())
                .name(user.getName())
                .isAdmin(user.getIsAdmin())
                .isBanned(user.getIsBanned())
                .score(user.getScore())
                .build();

        return userDTO;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/deleteUser/{id}")
    @ResponseBody
    public String deleteUser(@PathVariable(name = "id") long id) {
        return userService.deleteUser(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/saveUser")
    @ResponseBody
    public UserDTO saveUser(@RequestBody UserDTO userDto) {
        User user = User.builder()
                .username(userDto.getUsername())
                .password(userDto.getPassword())
                .name(userDto.getName())
                .isAdmin(userDto.getIsAdmin())
                .isBanned(userDto.getIsBanned())
                .score(userDto.getScore())
                .build();
        User savedUser = userService.saveUser(user);

        UserDTO savedUserDTO = UserDTO.builder()
                .id(savedUser.getId())
                .username(savedUser.getUsername())
                .password(savedUser.getPassword())
                .name(savedUser.getName())
                .isAdmin(savedUser.getIsAdmin())
                .isBanned(savedUser.getIsBanned())
                .score(savedUser.getScore())
                .build();

        return savedUserDTO;
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/updateUser")
    @ResponseBody
    public UserDTO updateUser(@RequestBody UserDTO userDto) {
        User user = User.builder()
                .id(userDto.getId())
                .username(userDto.getUsername())
                .password(userDto.getPassword())
                .name(userDto.getName())
                .isAdmin(userDto.getIsAdmin())
                .isBanned(userDto.getIsBanned())
                .score(userDto.getScore())
                .build();
        User updatedUser = userService.updateUser(user.getId(), user);

        UserDTO updatedUserDTO = UserDTO.builder()
                .id(updatedUser.getId())    
                .username(updatedUser.getUsername())
                .password(updatedUser.getPassword())
                .name(updatedUser.getName())
                .isAdmin(updatedUser.getIsAdmin())
                .isBanned(updatedUser.getIsBanned())
                .score(updatedUser.getScore())
                .build();

        return updatedUserDTO;
    }

    @PostMapping("/login")
    @ResponseBody
    public UserDTO login(@RequestBody LoginDTO loginDTO) throws Exception {
        return userService.login(loginDTO);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/banUser/{id}")
    public ResponseEntity<?> banUser(@PathVariable(name = "id") long id){
        userService.banUser(id);
        return ResponseEntity.ok("Success");
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/reduceScore/{id}")
    public ResponseEntity<?> reduceScore(@PathVariable(name = "id") long id){
        userService.reduceUserScore(id);
        return ResponseEntity.ok("Success");
    }
}
