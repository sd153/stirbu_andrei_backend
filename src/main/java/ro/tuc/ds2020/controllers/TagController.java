package ro.tuc.ds2020.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import ro.tuc.ds2020.dto.TagDTO;
import ro.tuc.ds2020.entities.Tag;
import ro.tuc.ds2020.services.TagService;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/tag")
@RequiredArgsConstructor
@CrossOrigin
public class TagController {

    private final TagService tagService;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    @ResponseBody
    public List<TagDTO> getTag() {
        List<Tag> tags = tagService.getAllTags();
        List<TagDTO> tagDTOS = new ArrayList<>();

        for(Tag tag : tags) {
            TagDTO tagDTO = TagDTO.builder()
            .id(tag.getId())
            .tagName(tag.getTagName())
            .build();

            tagDTOS.add(tagDTO);
        }

        return  tagDTOS;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getTag/{id}")
    @ResponseBody
    public TagDTO getTag(@PathVariable(name = "id") long id) {
        Tag tag = tagService.getTag(id);
        TagDTO tagDTO = TagDTO.builder()
                .id(tag.getId())
                .tagName(tag.getTagName())
                .build();

        return tagDTO;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/deleteTag/{id}")
    @ResponseBody
    public String deleteTag(@PathVariable(name = "id") long id) {
        return tagService.deleteTag(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/saveTag")
    @ResponseBody
    public TagDTO saveTag(@RequestBody TagDTO tagDTO) {
        Tag tag = Tag.builder()
                .tagName(tagDTO.getTagName())
                .build();
        Tag savedTag =  tagService.saveTag(tag);

        TagDTO savedTagDTO = TagDTO.builder()
                .id(savedTag.getId())
                .tagName(savedTag.getTagName())
                .build();

        return savedTagDTO;
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/updateTag")
    @ResponseBody
    public TagDTO updateTag(@RequestBody TagDTO tagDTO){
        Tag tag = Tag.builder()
                .id(tagDTO.getId())
                .tagName(tagDTO.getTagName())
                .build();
        Tag updatedTag = tagService.updateTag(tag.getId(),tag);

        TagDTO updatedTagDTO = TagDTO.builder()
                .id(updatedTag.getId())
                .tagName(updatedTag.getTagName())
                .build();

        return updatedTagDTO;
    }
}
