package ro.tuc.ds2020.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dto.QuestionDTO;
import ro.tuc.ds2020.dto.UserDTO;
import ro.tuc.ds2020.entities.Question;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.services.QuestionService;
import ro.tuc.ds2020.services.UserService;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/question")
@RequiredArgsConstructor
@CrossOrigin
public class QuestionController {

    private final QuestionService questionService;
    private final UserService userService;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    @ResponseBody
    public List<QuestionDTO> getQuestions() {
        List<Question> questions = questionService.getAllQuestions();
        List<QuestionDTO> questionDtos = new ArrayList<>();

        for(Question question : questions) {
            QuestionDTO questionDto = QuestionDTO.builder()
                    .id(question.getId())
                    .questionText(question.getQuestionText())
                    .userID(question.getUserQuestion().getId())
                    .tagList(question.getTagList())
                    .questionDate(question.getQuestionDate())
                    .questionVotes(question.getQuestionVotes())
                    .title(question.getTitle())
                    .build();

            questionDtos.add(questionDto);
        }

        return questionDtos;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getQuestion/{id}")
    @ResponseBody
    public QuestionDTO getQuestion(@PathVariable(name = "id") long id) {
        Question question = questionService.getQuestion(id);
        QuestionDTO questionDto = QuestionDTO.builder()
                .id(question.getId())
                .questionText(question.getQuestionText())
                .userID(question.getUserQuestion().getId())
                .tagList(question.getTagList())
                .questionDate(question.getQuestionDate())
                .questionVotes(question.getQuestionVotes())
                .title(question.getTitle())
                .build();

        return questionDto;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAllByUserID/{user_id}")
    @ResponseBody
    public List<QuestionDTO> getQuestionsByUserID(@PathVariable(name = "user_id") long user_id) {
        List<Question> questions = questionService.getAllQuestionsByUserID(user_id);
        List<QuestionDTO> questionDtos = new ArrayList<>();

        for(Question question : questions) {
            QuestionDTO questionDto = QuestionDTO.builder()
                    .id(question.getId())
                    .questionText(question.getQuestionText())
                    .userID(question.getUserQuestion().getId())
                    .tagList(question.getTagList())
                    .questionDate(question.getQuestionDate())
                    .questionVotes(question.getQuestionVotes())
                    .title(question.getTitle())
                    .build();

            questionDtos.add(questionDto);
        }

        return questionDtos;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/deleteQuestion/{id}")
    @ResponseBody
    public String deleteQuestion(@PathVariable(name = "id") long id) {
        return questionService.deleteQuestion(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/saveQuestion")
    @ResponseBody
    public QuestionDTO saveQuestion(@RequestBody QuestionDTO questionDTO) {
        User user = userService.getUser(questionDTO.getUserID());
        Question question = Question.builder()
                .questionText(questionDTO.getQuestionText())
                .userQuestion(user)
                .tagList(questionDTO.getTagList())
                .questionDate(questionDTO.getQuestionDate())
                .questionVotes(questionDTO.getQuestionVotes())
                .title(questionDTO.getTitle())
                .build();
        Question savedQuestion = questionService.saveQuestion(question);

        QuestionDTO savedQuestionDto = QuestionDTO.builder()
                .id(savedQuestion.getId())
                .questionText(savedQuestion.getQuestionText())
                .userID(savedQuestion.getUserQuestion().getId())
                .tagList(savedQuestion.getTagList())
                .questionDate(savedQuestion.getQuestionDate())
                .questionVotes(savedQuestion.getQuestionVotes())
                .title(savedQuestion.getTitle())
                .build();

        return savedQuestionDto;
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/updateQuestion")
    @ResponseBody
    public QuestionDTO updateQuestion(@RequestBody QuestionDTO questionDTO) {
        User user = userService.getUser(questionDTO.getUserID());
        Question question = Question.builder()
                .id(questionDTO.getId())
                .questionText(questionDTO.getQuestionText())
                .userQuestion(user)
                .tagList(questionDTO.getTagList())
                .questionDate(questionDTO.getQuestionDate())
                .questionVotes(questionDTO.getQuestionVotes())
                .title(questionDTO.getTitle())
                .build();
        Question updatedQuestion = questionService.updateQuestion(question.getId(), question);

        QuestionDTO updatedQuestionDto =  QuestionDTO.builder()
                .id(updatedQuestion.getId())
                .questionText(updatedQuestion.getQuestionText())
                .userID(updatedQuestion.getUserQuestion().getId())
                .tagList(updatedQuestion.getTagList())
                .questionDate(updatedQuestion.getQuestionDate())
                .questionVotes(updatedQuestion.getQuestionVotes())
                .title(updatedQuestion.getTitle())
                .build();

        return updatedQuestionDto;
    }
}
