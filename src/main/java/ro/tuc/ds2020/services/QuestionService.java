package ro.tuc.ds2020.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.entities.Question;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.repositories.QuestionRepo;
import ro.tuc.ds2020.repositories.UserRepo;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class QuestionService {

    private final QuestionRepo questionRepo;
    private final UserRepo userRepo;

    public List<Question> getAllQuestions() {
        return questionRepo.findAll();
    }

    public List<Question> getAllQuestionsByUserID(long userID) {
        return questionRepo.findAll().stream().filter(question -> question.getUserQuestion().getId() == userID).collect(Collectors.toList());
    }

    public Question getQuestion(long id) {
        Optional<Question> question = questionRepo.findById(id);

        return question.orElse(null);
    }

    public String deleteQuestion(long id) {
        try {
            questionRepo.delete(this.getQuestion(id));

            return "Delete success!";
        } catch (Exception e) {
            return "Delete failed.";
        }
    }

    public Question saveQuestion(Question question) {
        return questionRepo.save(question);
    }

    public Question updateQuestion(long id, Question question) {
        Question initialQuestion = this.getQuestion(id);
        initialQuestion.setTitle(question.getTitle());
        initialQuestion.setQuestionText(question.getQuestionText());
        initialQuestion.setQuestionDate(question.getQuestionDate());
        if(!question.getTagList().isEmpty()){
            initialQuestion.setTagList(question.getTagList());
        }

        User user = question.getUserQuestion();
        int userPoints = question.getQuestionVotes() - initialQuestion.getQuestionVotes();

        if(userPoints == 0) {
            return questionRepo.save(initialQuestion);
        }

        if(userPoints < 0){
            user.setScore(user.getScore() + userPoints * 2);
        } else {
            user.setScore(user.getScore() + userPoints * 5);
        }

        userRepo.save(user);

        initialQuestion.setQuestionVotes(question.getQuestionVotes());
        return questionRepo.save(initialQuestion);
    }
}
