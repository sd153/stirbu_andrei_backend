package ro.tuc.ds2020.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.entities.Answer;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.repositories.AnswerRepo;
import ro.tuc.ds2020.repositories.UserRepo;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class AnswerService {

    private final AnswerRepo answerRepo;
    private final UserRepo userRepo;

    public List<Answer> getAllAnswers() {
        return answerRepo.findAll();
    }

    public List<Answer> getAllAnswersByQuestionId(long questionId) {
        return answerRepo.findAll()
                .stream()
                .filter(answer -> answer.getQuestion().getId() == questionId)
                .collect(Collectors.toList());
    }

    public Answer getAnswer(long id) {
        Optional<Answer> question = answerRepo.findById(id);

        return question.orElse(null);
    }

    public String deleteAnswer(long id) {
        try {
            answerRepo.delete(this.getAnswer(id));

            return "Delete success!";
        } catch (Exception e) {
            return "Delete failed.";
        }
    }

    public Answer saveAnswer(Answer answer) {
        return answerRepo.save(answer);
    }

    public Answer updateAnswer(long id, Answer answer) {
        Answer initialAnswer = this.getAnswer(id);
        initialAnswer.setAnswerText(answer.getAnswerText());
        initialAnswer.setAnswerDate(answer.getAnswerDate());

        User user = answer.getUserAnswer();
        int userPoints = answer.getAnswerVotes() - initialAnswer.getAnswerVotes();

        if(userPoints == 0) {
            return answerRepo.save(initialAnswer);
        }

        if(userPoints < 0){
            user.setScore(user.getScore() + userPoints * 2);
        } else {
            user.setScore(user.getScore() + userPoints * 10);
        }

        userRepo.save(user);

        initialAnswer.setAnswerVotes(answer.getAnswerVotes());
        return answerRepo.save(initialAnswer);
    }

    public long countAnswersByQuestionId(long questionId) {
        return answerRepo.findAll().stream().filter(a -> a.getQuestion().getId() == questionId).count();
    }
}
