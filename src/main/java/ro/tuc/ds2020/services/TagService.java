package ro.tuc.ds2020.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.entities.Tag;
import ro.tuc.ds2020.repositories.TagRepo;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TagService {

    private final TagRepo tagRepo;

    public List<Tag> getAllTags() {
        return tagRepo.findAll();
    }

    public Tag getTag(long id) {
        Optional<Tag> tag = tagRepo.findById(id);

        return tag.orElse(null);
    }

    public String deleteTag(long id) {
        try {
            tagRepo.delete(this.getTag(id));

            return "Delete success!";
        } catch (Exception e) {
            return "Delete failed.";
        }
    }

    public Tag saveTag(Tag tag) {
        return tagRepo.save(tag);
    }

    public Tag updateTag(long id, Tag tag) {
        Tag initialTag = this.getTag(id);
        initialTag.setTagName(tag.getTagName());

        return tagRepo.save(initialTag);
    }
}
