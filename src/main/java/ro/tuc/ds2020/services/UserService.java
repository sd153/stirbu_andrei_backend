package ro.tuc.ds2020.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.tuc.ds2020.dto.LoginDTO;
import ro.tuc.ds2020.dto.UserDTO;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.repositories.UserRepo;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepo userRepo;

    @Transactional
    public List<User> getAllUsers() {
        return userRepo.findAll();
    }

    @Transactional
    public User getUser(long id) {
        Optional<User> user = userRepo.findById(id);

        return user.orElse(null);
    }

    public String deleteUser(long id) {
        try {
            userRepo.delete(this.getUser(id));

            return "Delete success!";
        } catch (Exception e) {
            return "Delete failed.";
        }
    }

    public User saveUser(User user) {
        return userRepo.save(user);
    }

    public User updateUser(long id, User user) {

        User initialUser = this.getUser(id);
        initialUser.setName(user.getName());
        initialUser.setUsername(user.getUsername());
        initialUser.setIsAdmin(user.getIsAdmin());
        initialUser.setIsBanned(user.getIsBanned());
        initialUser.setPassword(user.getPassword());
        initialUser.setScore(user.getScore());

        return userRepo.save(initialUser);
    }

    public void banUser(long id) {
        User initialUser = this.getUser(id);
        initialUser.setIsBanned(true);

        userRepo.save(initialUser);
    }

    public void reduceUserScore(long id){
        User initialUser = this.getUser(id);
        initialUser.setScore(initialUser.getScore() - 1);

        userRepo.save(initialUser);
    }

    public UserDTO login(LoginDTO loginDTO) throws Exception {
        Optional<User> userOptional = userRepo.findByUsername(loginDTO.getUsername());

        if(!userOptional.isPresent()) {
            throw new Exception("Cannot find user by name: " + loginDTO.getUsername());
        }

        User user = userOptional.get();
        if(!user.getPassword().equals(loginDTO.getPassword())){
            throw new Exception("Invalid credentials");
        }

        return UserDTO.builder()
                .id(user.getId())
                .name(user.getName())
                .username(user.getUsername())
                .isAdmin(user.getIsAdmin())
                .isBanned(user.getIsBanned())
                .score(user.getScore())
                .build();
    }
}
