package ro.tuc.ds2020.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "username", unique = true)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "isAdmin", nullable = false)
    private Boolean isAdmin;

    @Column(name = "isBanned", nullable = false)
    private Boolean isBanned;

    @Column(name = "score", nullable = false)
    private int score;
}
