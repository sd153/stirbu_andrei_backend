package ro.tuc.ds2020.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Question implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "questionText", nullable = false)
    private String questionText;

    @Column(name = "questionDate", nullable = false)
    private LocalDate questionDate;

    @Column(name = "questionVotes", nullable = false)
    private int questionVotes;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = true)
    private User userQuestion;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "questionTag",
            joinColumns = @JoinColumn(name = "questionId"),
            inverseJoinColumns = @JoinColumn(name = "tagId")
    )
    private List<Tag> tagList;
}
