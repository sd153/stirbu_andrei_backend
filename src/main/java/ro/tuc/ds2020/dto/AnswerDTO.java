package ro.tuc.ds2020.dto;

import lombok.*;

import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AnswerDTO {
    private long id;
    private String answerText;
    private LocalDate answerDate;
    private int answerVotes;
    private long userID;
    private long questionID;
}
