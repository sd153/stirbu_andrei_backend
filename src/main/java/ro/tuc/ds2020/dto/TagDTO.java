package ro.tuc.ds2020.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TagDTO {
    private long id;
    private String tagName;
}
