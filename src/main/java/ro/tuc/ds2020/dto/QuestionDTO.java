package ro.tuc.ds2020.dto;

import lombok.*;
import ro.tuc.ds2020.entities.Tag;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QuestionDTO {
    private long id;
    private String title;
    private String questionText;
    private LocalDate questionDate;
    private int questionVotes;
    private long userID;
    private List<Tag> tagList = new ArrayList<>();
}
