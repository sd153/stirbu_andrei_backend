package ro.tuc.ds2020.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    private long id;
    private String username;
    private String password;
    private String name;
    private Boolean isAdmin;
    private Boolean isBanned;
    private int score;
}
